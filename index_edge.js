/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0px', '0px', '1024px', '645px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'mapa',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"mapa.png",'0px','0px']
                            },
                            {
                                id: 'logo_metro',
                                type: 'group',
                                rect: ['23px', '576px', '88', '83px', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'm',
                                    type: 'image',
                                    rect: ['0px', '-32px', '66px', '112px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"m.svg",'0px','0px']
                                },
                                {
                                    id: 'metro',
                                    type: 'image',
                                    rect: ['70px', '1px', '80px', '28px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"metro.svg",'0px','0px']
                                },
                                {
                                    id: 'bogota',
                                    type: 'image',
                                    rect: ['70px', '21px', '80px', '31px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"bogota.svg",'0px','0px']
                                }]
                            },
                            {
                                id: 'area',
                                type: 'group',
                                rect: ['77px', '110px', '716px', '448px', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'linea',
                                    type: 'image',
                                    rect: ['315px', '-18px', '309px', '535px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"linea.svg",'0px','0px']
                                },
                                {
                                    id: 'b1',
                                    type: 'group',
                                    rect: ['410px', '412px', '25px', '43px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p1',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n1',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n1.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b2',
                                    type: 'group',
                                    rect: ['461px', '385px', '25px', '41px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p2',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n2',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n2.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b3',
                                    type: 'group',
                                    rect: ['492px', '366px', '26px', '39px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p3',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n3',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n3.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b4',
                                    type: 'group',
                                    rect: ['511px', '331px', '25px', '43px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p4',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n4',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n4.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b5',
                                    type: 'group',
                                    rect: ['502px', '306px', '24px', '39px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p5',
                                        type: 'image',
                                        rect: ['5px', '15px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n5',
                                        type: 'image',
                                        rect: ['0px', '-12px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n5.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b6',
                                    type: 'group',
                                    rect: ['506px', '258px', '26px', '41px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p6',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n6',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n6.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b7',
                                    type: 'group',
                                    rect: ['538px', '222px', '26px', '41px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p7',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n7',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n7.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b8',
                                    type: 'group',
                                    rect: ['568px', '186px', '25px', '40px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p8',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n8',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n8.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b9',
                                    type: 'group',
                                    rect: ['589px', '155px', '26px', '37px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p9',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n9',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n9.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b10',
                                    type: 'group',
                                    rect: ['600px', '89px', '27px', '43px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p10',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n10',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n10.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b11',
                                    type: 'group',
                                    rect: ['591px', '43px', '24px', '40px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p11',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n11',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n11.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b12',
                                    type: 'group',
                                    rect: ['562px', '23px', '27px', '39px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p12',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n12',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n12.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b13',
                                    type: 'group',
                                    rect: ['520px', '-2px', '26px', '37px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p13',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n13',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n13.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b14',
                                    type: 'group',
                                    rect: ['451px', '-23px', '24px', '37px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p14',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n14',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n14.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b15',
                                    type: 'group',
                                    rect: ['383px', '-35px', '26px', '38px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p15',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n15',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n15.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                },
                                {
                                    id: 'b16',
                                    type: 'group',
                                    rect: ['335px', '-48px', '24px', '37px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    userClass: "b pointer",
                                    c: [
                                    {
                                        id: 'p16',
                                        type: 'image',
                                        rect: ['5px', '29px', '17px', '17px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"p1.svg",'0px','0px'],
                                        userClass: "point"
                                    },
                                    {
                                        id: 'n16',
                                        type: 'image',
                                        rect: ['0px', '2px', '26px', '38px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"n16.svg",'0px','0px'],
                                        userClass: "btn"
                                    }]
                                }]
                            },
                            {
                                id: 'titulo',
                                type: 'image',
                                rect: ['844px', '593px', '103px', '31px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"titulo.svg",'0px','0px']
                            },
                            {
                                id: 'main',
                                type: 'group',
                                rect: ['-5', '1', '1045', '639', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'main-fondo',
                                    type: 'rect',
                                    rect: ['auto', '0px', '1045px', '639px', '0px', 'auto'],
                                    fill: ["rgba(0,0,0,0.30)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                },
                                {
                                    id: 'l1-btn-9',
                                    type: 'image',
                                    rect: ['auto', '512px', '195px', '67px', '823px', 'auto'],
                                    clip: 'rect(0px 195px 66px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-9.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-8',
                                    type: 'image',
                                    rect: ['auto', '458px', '261px', '67px', '757px', 'auto'],
                                    clip: 'rect(0px 261px 57px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-8.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-7',
                                    type: 'image',
                                    rect: ['auto', '407px', '273px', '68px', '745px', 'auto'],
                                    clip: 'rect(0px 273px 55px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-7.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-6',
                                    type: 'image',
                                    rect: ['auto', '355px', '240px', '68px', '778px', 'auto'],
                                    clip: 'rect(0px 240px 58px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-6.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-5',
                                    type: 'image',
                                    rect: ['auto', '305px', '243px', '67px', '775px', 'auto'],
                                    clip: 'rect(0px 243px 55px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-5.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-4',
                                    type: 'image',
                                    rect: ['auto', '255px', '170px', '67px', '848px', 'auto'],
                                    clip: 'rect(0px 170px 55px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l2-btn-4.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-3',
                                    type: 'image',
                                    rect: ['auto', '208px', '313px', '68px', '705px', 'auto'],
                                    clip: 'rect(0px 313px 60px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-3.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-2',
                                    type: 'image',
                                    rect: ['auto', '160px', '214px', '68px', '804px', 'auto'],
                                    clip: 'rect(0px 214px 58px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-2.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l1-btn-1',
                                    type: 'image',
                                    rect: ['auto', '113px', '261px', '67px', '757px', 'auto'],
                                    clip: 'rect(0px 261px 57px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l1-btn-1.svg",'0px','0px'],
                                    userClass: "l1"
                                },
                                {
                                    id: 'l2-btn-5',
                                    type: 'image',
                                    rect: ['auto', '304px', '244px', '67px', '29px', 'auto'],
                                    clip: 'rect(0px 244px 58px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l2-btn-5.svg",'0px','0px'],
                                    userClass: "l2"
                                },
                                {
                                    id: 'l2-btn-4',
                                    type: 'image',
                                    rect: ['auto', '251px', '462px', '68px', '28px', 'auto'],
                                    clip: 'rect(0px 462px 55px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l2-btn-42.svg",'0px','0px'],
                                    userClass: "l2"
                                },
                                {
                                    id: 'l2-btn-3',
                                    type: 'image',
                                    rect: ['auto', '201px', '348px', '67px', '27px', 'auto'],
                                    clip: 'rect(0px 348px 57px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l2-btn-3.svg",'0px','0px'],
                                    userClass: "l2"
                                },
                                {
                                    id: 'l2-btn-2',
                                    type: 'image',
                                    rect: ['auto', '153px', '256px', '67px', '25px', 'auto'],
                                    clip: 'rect(0px 256px 54px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l2-btn-2.svg",'0px','0px'],
                                    userClass: "l2"
                                },
                                {
                                    id: 'l2-btn-1',
                                    type: 'image',
                                    rect: ['auto', '106px', '311px', '68px', '25px', 'auto'],
                                    clip: 'rect(0px 311px 54px 0px)',
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"l2-btn-1.svg",'0px','0px'],
                                    userClass: "l2"
                                }]
                            },
                            {
                                id: 'btn_list_1',
                                type: 'image',
                                rect: ['auto', '63px', '259px', '67px', '742px', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_list_1.svg",'0px','0px']
                            },
                            {
                                id: 'btn_list_2',
                                type: 'image',
                                rect: ['auto', '59px', '340px', '68px', '8px', 'auto'],
                                clip: 'rect(0px 340px 54px 0px)',
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_list_2.svg",'0px','0px']
                            },
                            {
                                id: 'title',
                                type: 'image',
                                rect: ['auto', '11px', '923px', '66px', '62px', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"title.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'modal',
                            type: 'group',
                            rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'modal-fondo',
                                type: 'image',
                                rect: ['auto', '0px', '1024px', '640px', '0px', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"modal-fondo.svg",'0px','0px']
                            },
                            {
                                id: 'gallery',
                                type: 'rect',
                                rect: ['auto', '164px', '941px', '437px', '40px', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'header',
                                type: 'group',
                                rect: ['284px', '30px', '566px', '106px', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'modal-header',
                                    type: 'image',
                                    rect: ['auto', '-12px', '778px', '128px', '-52px', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"estaciones_titulo.png",'0px','0px']
                                },
                                {
                                    id: 'modal-direction',
                                    type: 'text',
                                    rect: ['-284px', '28px', '1024px', '13px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'modal-text',
                                    type: 'text',
                                    rect: ['-284px', '66px', '1024px', '13px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    align: "center",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(255,255,255,1.00)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'modal-title',
                                    type: 'rect',
                                    rect: ['163px', '-22px', '149px', '43px', 'auto', 'auto'],
                                    borderRadius: ["10px", "10px", "10px", "10px"],
                                    fill: ["rgba(128,70,128,1.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                }]
                            },
                            {
                                id: 'modal-close',
                                type: 'image',
                                rect: ['876px', '16px', '28px', '28px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"modal-close.svg",'0px','0px']
                            },
                            {
                                id: 'zoom',
                                type: 'group',
                                rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'zoom-fondo',
                                    type: 'rect',
                                    rect: ['auto', '0px', '1024px', '640px', '0px', 'auto'],
                                    fill: ["rgba(43,43,43,0.53)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'container-slider',
                                    type: 'group',
                                    rect: ['67', '30', '892', '586', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'slider',
                                        type: 'rect',
                                        rect: ['auto', '0px', '892px', '586px', '0px', 'auto'],
                                        fill: ["rgba(43,43,43,0.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"]
                                    }]
                                },
                                {
                                    id: 'atr',
                                    type: 'text',
                                    rect: ['auto', '294px', 'auto', 'auto', '965px', 'auto'],
                                    cursor: 'pointer',
                                    text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255); font-size: 88px;\">&lt;</span></p>",
                                    align: "left",
                                    font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'sig',
                                    type: 'text',
                                    rect: ['auto', '294px', 'auto', 'auto', '14px', 'auto'],
                                    cursor: 'pointer',
                                    text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255); font-size: 88px;\">&gt;</span></p>",
                                    align: "left",
                                    font: ['Arial, Helvetica, sans-serif', [24, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "nowrap"],
                                    textStyle: ["", "", "", "", "none"]
                                },
                                {
                                    id: 'zoom-close',
                                    type: 'image',
                                    rect: ['945px', '18px', '28px', '28px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"modal-close.svg",'0px','0px']
                                }]
                            }]
                        },
                        {
                            id: 'modal-pages',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'backgroud-modal',
                                type: 'rect',
                                rect: ['auto', '0px', '1024px', '640px', '0px', 'auto'],
                                fill: ["rgba(192,192,192,0.33)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'page',
                                type: 'rect',
                                rect: ['auto', '26px', '969px', '597px', '27px', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'modal--close',
                                type: 'rect',
                                rect: ['auto', '7px', '36px', '36px', '5px', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024px', '640px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'm11',
                                type: 'image',
                                rect: ['489px', '287px', '36px', '36px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"m.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                        [
                            "eid15785",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15740",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15735",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15787",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15761",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15755",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15741",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15793",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15750",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15746",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15796",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15742",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15765",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15737",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15756",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15748",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15762",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid36",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138041",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138042",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138043",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138044",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138045",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15743",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15754",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15744",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15753",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n15}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15745",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid43",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138046",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138047",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138048",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138049",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138050",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid44",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138051",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138052",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138053",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138054",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138055",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15786",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15769",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid37",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138056",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138057",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138058",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138059",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138060",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15752",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15749",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid39",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138061",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138062",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138063",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138064",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138065",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid41",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138066",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138067",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138068",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138069",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138070",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid49",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138071",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138072",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138073",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138074",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138075",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15751",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid45",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138076",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138077",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138078",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138079",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138080",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15736",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid46",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138081",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138082",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138083",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138084",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138085",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15764",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n14}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15758",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15782",
                            "left",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid15795",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15790",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15780",
                            "left",
                            0,
                            0,
                            "linear",
                            "${n2}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid15797",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15763",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15792",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid42",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138086",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138087",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138088",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138089",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138090",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15791",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid35",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138091",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138092",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138093",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138094",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138095",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15738",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n9}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid47",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138096",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138097",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138098",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138099",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138100",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n4}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15784",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15760",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n6}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15757",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n10}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15789",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n16}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15739",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid15766",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid48",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138101",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138102",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138103",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138104",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138105",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n3}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15759",
                            "height",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            '38px',
                            '38px'
                        ],
                        [
                            "eid15788",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n7}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid15783",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n12}",
                            '2px',
                            '2px'
                        ],
                        [
                            "eid40",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138106",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138107",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138108",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138109",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138110",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n11}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid38",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138111",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138112",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138113",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138114",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138115",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n13}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid15747",
                            "width",
                            0,
                            0,
                            "linear",
                            "${n8}",
                            '26px',
                            '26px'
                        ],
                        [
                            "eid50",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138116",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138117",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138118",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138119",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid138120",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${n1}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid18680",
                            "top",
                            0,
                            0,
                            "linear",
                            "${n5}",
                            '-12px',
                            '-12px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
