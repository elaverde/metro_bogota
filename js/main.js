var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Metro Bogotá",
    autor: "Edilson Laverde Molina",
    date: "31-10-2022",
    email: "edilsonlaverde_182@hotmail.com",
    icon: './images/m.svg',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];
var contenidos = [
{
    title: "Estación 1",
    color: "#308253",
    text: "Carrera 96",
    direction: "Av. Villavicencio/Carrera 94 - Carrera 93",
    images:4,
    index:1
},{
    title: "Estación 2",
    color: "#308253",
    text: "Portal Américas",
    direction: "Av. Villavicencio/Carrera 86B - Carrera 86G",
    images:4,
    index:2
},
{
    title: "Estación 3",
    color: "#804680",
    direction: "Av. Villavicencio/Carrera 80D - Carrera 80G",
    text: "Carrera 80",
    images:4,
    index:3
},
{
    title: "Estación 4",
    color: "#804680",
    direction: "Av. Primera de Mayo/Calle 42 sur - Calle 42C sur",
    text: "Calle 42 Sur",
    images:4,
    index:4
},
{
    title: "Estación 5",
    color: "#804680",
    direction: "Av. Primera de Mayo/Calle 40 sur - Calle 39 sur",
    text: "Kennedy <br> (avenida Primero de Mayo – Calle 39 C)",
    images:4,
    index:5
},
{
    title: "Estación 6",
    color: "#956145",
    direction: "Av. Primera de Mayo/Av. Boyacá - Carrera 72C",
    text: "Avenida Boyacá",
    images:4,
    index:6
},
{
    title: "Estación 7",
    color: "#956145",
    direction: "Av. Primera de Mayo/Av. 68 - Carrera 52C sur",
    text: "Avenida 68",
    images:4,
    index:7
},
{
    title: "Estación 8",
    color: "#956145",
    direction: "NQS/Diagonal 16 sur - Calle 17A BIS sur",
    text: "Carrera 50",
    images:4,
    index:8
},
{
    title: "Estación 9",
    color: "#A73080",
    direction: "Calle 1/Carrera 24 - Carrera 24C",
    text: "Avenida Norte Quito Sur NQS",
    images:4,
    index:9
},
{
    title: "Estación 10",
    color: "#A73080",
    direction: "Av. Caracas/Calle 2 - Calle 3",
    text: "Carrera 24",
    images:4,
    index:10
},
{
    title: "Estación 11",
    color: "#A73080",
    direction: "Av. Caracas/Calle 11 - Calle 13",
    text: "Av. Caracas con Calle 1",
    images:4,
    index:11
},
{
    title: "Estación 12",
    color: "#8D8530",
    direction: "Av. Caracas/Calle 24A - Calle 26",
    text: "Calle 10",
    images:4,
    index:12
},
{
    title: "Estación 13",
    color: "#8D8530",
    direction: "Av. Caracas/Calle 42 - Calle 44",
    text: "Calle 26",
    images:4,
    index:13
},
{
    title: "Estación 14",
    color: "#8D8530",
    direction: "Av. Caracas/Calle 61 - Calle 63",
    text: "Calle 45",
    images:4,
    index:14
},
{
    title: "Estación 15",
    color: "#98566A",
    direction: "Av. Caracas/Calle 72 - Calle 74",
    text: "Calle 63",
    images:4,
    index:15
},
{
    title: "Estación 16",
    color: "#98566A",
    direction: "Patio taller",
    text: "Calle 72",
    images:4,
    index:16
},

]

template_postions = [{
    name: "template4",
    positions:[{x:"10px",y:"30px"},{x:"450px",y:"0px"},{x:"450px",y:"180px"},{x:"720px",y:"30px"},{x:"720px",y:"210px"}]
}];
var colIframe="#000"
function colorIframe(){
    var iframe = document.getElementById('iframe');
    iframe.contentWindow.document.documentElement.style.setProperty('--color-principal', colIframe);
}
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            //precarga audios//
            t.animation();
            t.events();
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.timeScale(1).play();
            });
        },
        methods: {
            load_zoom: function (index,value) {
                $(ST + "slider").html("");
                let nIndex = contenidos[index].index;
                $(ST + "slider").append(`<img id="s0" class="sliders" src='images/estaciones/estacion_${nIndex}/estacion_${nIndex}.png' >`);

                for (let i = 1; i <= contenidos[index].images; i++) {
                    $(ST + "slider").append(`<img id="s${i}" class="sliders" src='images/estaciones/estacion_${nIndex}/${i}.jpg' >`);
                }
                 //configuración slider principal//
                var slider=""; 
                //destruimos los eventos//
                $(ST+"sig").unbind();
                $(ST+"atr").unbind();
                slider=ivo(ST+"slider").slider({
                    slides:'.sliders',
                    btn_next:ST+"sig",
                    btn_back:ST+"atr",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        ivo.play("clic");
                    },
                    onFinish:function(){
                    }
                });
                slider.go(value);

            },
            layaud_html: function (index) {
                let set_title = function (index) {
                    ivo(ST + "modal-title").html(contenidos[index].title);
                }
                let set_direction = function (index) {
                    ivo(ST + "modal-direction").html(contenidos[index].direction);
                }
                let set_text = function (index) {
                    ivo(ST + "modal-text").html(contenidos[index].text);
                }
                let set_color = function (index) {
                    ivo(ST + "modal-title").css("background", contenidos[index].color);
                }
                index = parseInt(index)-1;
                set_title(index);
                set_color(index);
                set_direction(index);
                set_text(index);

                //cargamos la galeria de imagenes//
                let nIndex = contenidos[index].index;
                //asinamos el template
                /*let template = {};
                for (let i = 0; i < 1 ; i++) {
                    if("template"+i == template_postions[i].name){
                       template = template_postions[i].positions;
                    }
                }*/
                let template = template_postions[0].positions;
                $(ST + "gallery").html("");
                $(ST + "gallery").append(`
                    <div data-value='0'  data-index="${index}" class="gallery-principal" style="position:absolute; top: ${template[0].y}; left: ${template[0].x}; width:400px; height:230px; background-image:url('images/estaciones/estacion_${nIndex}/estacion_${nIndex}.png');" >
                        <span>Fuente: WSP</span>
                    </div>
                `);
                for (let i = 4; i >= 1; i--) {
                    $(ST + "gallery").append(`
                        <div data-value='${i}' data-index="${index}" class="gallery-secundarias" style="position:absolute; top: ${template[i].y}; left: ${template[i].x}; width:250px; height:200px; background-image:url('images/estaciones/estacion_${nIndex}/${i}.jpg');" >
                            <span>Fuente: Consorcio Ambiental Metro Línea Bogotá 1</span>
                        </div>
                    `);
                }
                $(".gallery-principal, .gallery-secundarias").on("click", function () {
                    let index = $(this).attr("data-index");
                    let value = $(this).attr("data-value");
                    t.load_zoom(index,value);
                    zoom.play();
                    ivo.play("clic");
                });

            },
            setColor: function (color) {
                //cambiamos el color de la clase//
                var cls1 = document.getElementsByClassName("cls-1");
                cls1[0].style.stroke=color;
                var cls2 = document.getElementsByClassName("cls-2");
                cls2[0].style.fill=color;
                //cambios de color la variable css --color-principal//
                document.documentElement.style.setProperty('--color-principal', color); 
                //lamamos la funcion del iframe//
                var iframe = document.getElementById('iframe');
                //cambios de color la variable css --color-principal del iframe//
                colIframe = color;
                //creamos un intervalo lo ejecutamos hasta que --color-principal sea igual a color establecemos segundo de verificacion
                var interval = setInterval(function(){
                    if(iframe.contentWindow.document.documentElement.style.getPropertyValue('--color-principal') == color){
                        clearInterval(interval);
                    }
                    iframe.contentWindow.document.documentElement.style.setProperty('--color-principal', color);
                }, 1000);

            },
            events: function () {
                var t = this;
                //Stage_modal--close
                ivo("#Stage_modal--close").html(`
                <?xml version="1.0" encoding="UTF-8"?>
                    <svg id="subventana" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 27.84 27.84">
                    <defs>
                        <style>
                        .cls-1 {
                            fill: none;
                            stroke: #24229e;
                            stroke-miterlimit: 10;
                            stroke-width: .96px;
                        }

                        .cls-2 {
                            fill: #24229e;
                        }

                        .cls-3 {
                            fill: #fff;
                            filter: url(#drop-shadow-1);
                        }
                        </style>
                        <filter id="drop-shadow-1" filterUnits="userSpaceOnUse">
                        <feOffset dx=".96" dy=".96"/>
                        <feGaussianBlur result="blur" stdDeviation=".96"/>
                        <feFlood flood-color="#000" flood-opacity=".35"/>
                        <feComposite in2="blur" operator="in"/>
                        <feComposite in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <circle class="cls-3" cx="12.93" cy="12.97" r="10.93"/>
                    <g>
                        <path class="cls-2" d="M12.93,1.8c6.16,0,11.17,5.01,11.17,11.17s-5.01,11.17-11.17,11.17S1.76,19.13,1.76,12.97,6.77,1.8,12.93,1.8Zm0,21.59c5.74,0,10.41-4.67,10.41-10.41S18.67,2.56,12.93,2.56,2.52,7.23,2.52,12.97s4.67,10.41,10.41,10.41Z"/>
                        <path class="cls-1" d="M15.61,8.7l-2.67,2.68-2.67-2.67c-.44-.44-1.16-.44-1.6,0-.44,.44-.44,1.16,0,1.6l2.67,2.67-2.67,2.67c-.44,.44-.44,1.16,0,1.6,.44,.44,1.16,.44,1.6,0l2.67-2.67,2.67,2.67c.44,.44,1.16,.44,1.6,0,.44-.44,.44-1.16,0-1.6l-2.67-2.67,2.67-2.68c.44-.44,.44-1.16,0-1.6-.44-.44-1.16-.44-1.6,0h0Z"/>
                    </g>
                    </svg>
                `);
                ivo(".btn").css("cursor","pointer");
                ivo(".b").on("click", function () {
                    ivo.play("clic");
                    //obtenemos el n del id 
                    var n = this.id.split(S+"b")[1];
                    t.layaud_html(n);
                    modal.timeScale(1).play();
                })
                .on("mouseover", function () {
                   // this.css("opacity", .8);
                   var n = this.id.split(S+"b")[1];
                   window["btn"+n].play();
                })
                .on("mouseout", function () {
                    var n = this.id.split(S+"b")[1];
                   window["btn"+n].reverse();
                });

                //cerrar el modal//
                ivo(ST + "modal-close").on("click", function () {
                    ivo.play("clic");
                    modal.timeScale(10).reverse();
                });

                //cerrar el zoom//
                ivo(ST + "zoom-close").on("click", function () {
                    ivo.play("clic");
                    zoom.timeScale(10).reverse();
                });
                //cerrar el zoom//
                ivo(ST + "modal--close").on("click", function () {
                    ivo.play("clic");
                    modalPage.timeScale(10).reverse();
                });

                //hover de main//
                ivo(ST + "btn_list_1").on("click", function () {
                    ivo(ST + "page").html(`
                        <iframe onload="colorIframe" class="iframe" id="iframe"  width="100%" height="100%" src="./templates/page1-1.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe class="iframe" id="iframe">
                    `);
                    modalPage.timeScale(1).play();
                    t.setColor("#8c1f8c");

                }) 
                .on("mouseover", function () {
                    // this.css("opacity", .8);
                    lista1.timeScale(1).play();
                })
                .on("mouseout", function () {
                }).css({"cursor":"pointer"});
                ivo(ST + "btn_list_2").on("click", function () {
                    ivo(ST + "page").html(`
                        <iframe onload="colorIframe" class="iframe" id="iframe"  width="100%" height="100%" src="./templates/page2-1.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe class="iframe" id="iframe">
                    `);
                    modalPage.timeScale(1).play();
                    t.setColor("#00A7DE");

                }) 
                .on("mouseover", function () {
                    // this.css("opacity", .8);
                    lista2.timeScale(1).play();
                })
                .on("mouseout", function () {
                }).css({"cursor":"pointer"});
                ivo(ST + "main-fondo").on("click", function () {}) 
                .on("mouseover", function () {
                    // this.css("opacity", .8);
                    lista2.timeScale(5).reverse();
                    lista1.timeScale(5).reverse();
                })
                .on("mouseout", function () {
                });
                //hover bótones de main//
                ivo(".l1").on("click", function () {
                    let id = this.id.split("Stage_l1-btn-")[1];
                    id = parseInt(id)+1;
                    //agregamos el iframe class="iframe" id="iframe"//
                    ivo(ST + "page").html(`
                        <iframe onload="colorIframe" class="iframe" id="iframe"  width="100%" height="100%" src="./templates/page1-${id}.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe class="iframe" id="iframe">
                    `);
                    switch (id) {
                        case 2:
                            t.setColor("#D01A91");
                            break;
                        case 3:
                            t.setColor("#E06A1B");
                            break;
                        case 4:
                            t.setColor("#E39E2D");
                            break;
                        case 5:
                            t.setColor("#B1D121");
                            break;
                        case 6:
                            t.setColor("#31A800");
                            break;
                        case 7:
                            t.setColor("#00A7DE");
                            break;
                        case 8:
                            t.setColor("#3E6E9F");
                            break;
                        case 9:
                            t.setColor("#FFB900");
                            break;
                        case 10:
                            t.setColor("#DE1400");
                            break;

                    }

                    

                    modalPage.timeScale(1).play();
                });
                ivo(".l2").on("click", function () {
                    let id = this.id.split("Stage_l2-btn-")[1];
                    id = parseInt(id)+1;
                    //agregamos el iframe class="iframe" id="iframe"//
                    ivo(ST + "page").html(`
                        <iframe onload="colorIframe" class="iframe" id="iframe" width="100%" height="100%" src="./templates/page2-${id}.html" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe class="iframe" id="iframe">
                    `);
                    //cambiamos el color de la clase//
                    var cls1 = document.getElementsByClassName("cls-1");
                    cls1[0].style.stroke="red";
                    var cls2 = document.getElementsByClassName("cls-2");
                    cls2[0].style.fill="red";
                    console.log("maps");
                    modalPage.timeScale(1).play();
                    switch (id) {
                        case 2:
                            t.setColor("#31A800");
                            break;
                        case 3:
                            t.setColor("#E5A02D");
                            break;
                        case 4:
                            t.setColor("#D01A91");
                            break;
                        case 5:
                            t.setColor("#B1D121");
                            break;
                        case 6:
                            t.setColor("#FF6A1B");
                            break;
                     

                    }

                }).css("cursor","pointer");
            
            },
            animation: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "title", .8, {x: -1000, opacity: 0}), 0);
                
                stage1.append(TweenMax.staggerFrom(ST+"mapa", .4, {x: 100, y:300, opacity: 0,rotation:1900, scale: 10, ease: Elastic.easeOut.config(0.3, 0.8)}, -.2), 0);
                stage1.append(TweenMax.from(ST + "linea", .8, {x: -10, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".point", .4, {x: 100, opacity: 0,rotation:900, scale: 0, ease: Elastic.easeOut.config(0.3, 0.4)}, -.2),0);
                stage1.append(TweenMax.to(ST + "title", .8, {x: -1000, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "btn_list_1", .8, {x: -10, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "btn_list_2", .8, {x: -10, opacity: 0}), 0);
                
                //stage1.append(TweenMax.staggerFrom(".btn", .4, {x: 100, opacity: 0,rotation:900, scaleY: 0}, .3), -4);
                /*logo metro*/
                stage1.append(TweenMax.from(ST + "m", .8, {scale: 0, opacity: 0, rotation: 900}), 0);
                stage1.append(TweenMax.from(ST + "metro", .8, {x:100, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "bogota", .8, {x:100, opacity: 0}), -.3);
                stage1.append(TweenMax.from(ST + "titulo", .8, {x:100, opacity: 0}), -.3);
                stage1.stop();

                modal = new TimelineMax();
                modal.append(TweenMax.from(ST + "modal", .8, {y: -800, opacity: 0}), 0);
                modal.append(TweenMax.from(ST + "modal-title", .8, {y: 50, opacity: 0}), 0);
                modal.append(TweenMax.from(ST + "header", .8, {x: 800, opacity: 0}), 0);
                modal.append(TweenMax.from(ST + "gallery", .8, {x: 800, opacity: 0}), 0);
                modal.append(TweenMax.from(ST + "modal-close", .8, {scale: 0, opacity: 0, rotation: 900}), 0);
                modal.stop();

                modalPage = new TimelineMax();
                modalPage.append(TweenMax.from(ST + "modal-pages", .8, {y: -800, opacity: 0}), 0);
                modalPage.append(TweenMax.from(ST + "page", .8, {x: 800, opacity: 0}), 0);
                modalPage.append(TweenMax.from(ST + "modal--close", .8, {scale: 0, opacity: 0, rotation: 900}), 0);
                modalPage.stop();

                zoom = new TimelineMax();
                zoom.append(TweenMax.from(ST + "zoom", .8, {scale: 0, opacity: 0, rotation: 900}), 0);
                zoom.stop();

                for (var i = 1; i <= 16; i++) {
                    window["btn" + i] = new TimelineMax();
                    window["btn" + i].append(TweenMax.from(ST + "n" + i, .8, {scaleY: 0, opacity: 0}), 0);
                    window["btn" + i].stop();
                }

                lista1 = new TimelineMax();
                lista1.append(TweenMax.fromTo(ST + "main", .8, {y: -800, opacity: 0}, {y: 0, opacity: 1}), 0);
                lista1.append(TweenMax.staggerFrom(".l1", .2, {x: 500, opacity: 0, scale: 0}, -.1),-.4);
                lista1.stop();

                lista2 = new TimelineMax();
                lista2.append(TweenMax.fromTo(ST + "main", .8, {y: -800, opacity: 0}, {y: 0, opacity: 1}), 0);
                lista2.append(TweenMax.staggerFrom(".l2", .2, {x: 500, opacity: 0, scale: 0}, -.1),-.4);
                lista2.stop();
                

            }
        }
    });
}